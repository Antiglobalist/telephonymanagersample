package com.anthome.tms

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.anthome.tms.other.PermissionUtils.Companion.TELEPHONY_STATE_PERMISSIONS
import com.anthome.tms.other.PermissionUtils.Companion.checkPermissions
import com.anthome.tms.other.PermissionUtils.Companion.isPermissionGranted
import com.anthome.tms.other.isPermissionRequare
import com.anthome.tms.services.TelephonyService

class StarterActivity : AppCompatActivity() {

    private val STATE_PERMISSION_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starter)
        if (checkPermissions(this, TELEPHONY_STATE_PERMISSIONS)) {
            startTelephonyService()
            launchMainActivity()
        } else {
            requestPermissions()
        }
    }

    private fun requestPermissions() {
        isPermissionRequare {
            requestPermissions(TELEPHONY_STATE_PERMISSIONS, STATE_PERMISSION_REQUEST)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            STATE_PERMISSION_REQUEST -> {
                if (isPermissionGranted(grantResults)) {
                    startTelephonyService()
                    launchMainActivity()
                } else {
                    AlertDialog.Builder(this)
                            .setMessage(R.string.permissions_denied_text)
                            .setPositiveButton(R.string.retry, { d, w ->
                                requestPermissions()
                                d.dismiss()
                            })
                            .setNegativeButton(R.string.exit, { d, w ->
                                finish()
                                d.dismiss()
                            })
                            .create().show()
                }
                return
            }
        }
    }

    private fun startTelephonyService() {
        startService(Intent(this, TelephonyService::class.java))
    }

    private fun launchMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
