package com.anthome.tms.other

import android.os.Build


inline fun isPermissionRequare(code: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        code()
    }
}

inline fun <T:Any, R> notNull(input: T?, callback: (T)->R): R? {
    return input?.let(callback)
}