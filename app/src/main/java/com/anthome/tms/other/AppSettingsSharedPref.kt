package com.anthome.tms.other

import android.content.Context
import android.content.SharedPreferences

private const val SHARED_NAME: String  = "app settings"
private const val SHARED_FIELD_AUTOSTART: String  = "app settings"

class AppSettingsSharedPref : AppSettings {

    private val prefs: SharedPreferences

    constructor(context: Context){
        prefs = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)
    }

    override fun isAutoStartEnabled() = prefs.getBoolean(SHARED_FIELD_AUTOSTART, true)

    override fun setAutoStartEnable(enable: Boolean) {
       prefs.edit().putBoolean(SHARED_FIELD_AUTOSTART, enable).apply()
    }
}