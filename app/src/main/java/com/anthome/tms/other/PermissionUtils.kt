package com.anthome.tms.other

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.READ_PHONE_STATE
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.PermissionChecker

class PermissionUtils {
    companion object {
        val TELEPHONY_STATE_PERMISSIONS = arrayOf(
            READ_PHONE_STATE, ACCESS_COARSE_LOCATION
        )

        fun isPermissionGranted(grantResults: IntArray): Boolean {
            if (grantResults.size > 0) {
                val permissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (!permissionGranted) {
                    return false
                }
            }
            return true
        }

        fun checkPermissions(context: Context, permissions: Array<String>): Boolean {
            isPermissionRequare {
                for (permission in permissions) {
                    if (PermissionChecker.checkSelfPermission(
                            context,
                            permission
                        ) != PermissionChecker.PERMISSION_GRANTED
                    ) {
                        return false
                    }
                }
            }
            return true
        }
    }
}