package com.anthome.tms.other

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.anthome.tms.other.PermissionUtils.Companion.TELEPHONY_STATE_PERMISSIONS
import com.anthome.tms.other.PermissionUtils.Companion.checkPermissions
import com.anthome.tms.services.TelephonyService


class BootCompletedReceiver : BroadcastReceiver() {

    //Inject AppSettings
    private lateinit var settings: AppSettings

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context, intent: Intent) {
        settings = AppSettingsSharedPref(context)
        if(settings.isAutoStartEnabled()
                and checkPermissions(context, TELEPHONY_STATE_PERMISSIONS)){
            context.startService(Intent(context, TelephonyService::class.java))
        }
    }
}