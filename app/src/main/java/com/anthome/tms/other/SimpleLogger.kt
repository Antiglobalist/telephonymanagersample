package com.anthome.tms.other

import android.util.Log


class SimpleLogger : Logger {
    override fun log(simpleData: String) {
        Log.i("App message", simpleData)
    }
}