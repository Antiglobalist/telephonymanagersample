package com.anthome.tms.other


interface AppSettings {
    fun isAutoStartEnabled() : Boolean
    fun setAutoStartEnable(enable: Boolean)
}