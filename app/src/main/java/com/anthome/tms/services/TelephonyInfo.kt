package com.anthome.tms.services

import android.telephony.CellInfo
import android.telephony.CellLocation
import android.telephony.ServiceState
import android.telephony.SignalStrength
import java.io.Serializable


data class TelephonyInfo(
    val location: CellLocation? = null,
    val signalStrength: SignalStrength? = null,
    val cellInfo: MutableList<CellInfo>? = null,
    val serviceState: ServiceState? = null) : Serializable {
}