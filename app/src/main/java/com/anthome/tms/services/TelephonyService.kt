package com.anthome.tms.services

import android.annotation.SuppressLint
import android.app.Service
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import android.telephony.*
import android.telephony.PhoneStateListener.*
import com.anthome.tms.other.Logger
import com.anthome.tms.other.SimpleLogger


class TelephonyService : Service() {

    companion object {
        const val BROADCAST_NAME = "Telephony data changed"
        private const val BROADCAST_DATA_KEY = "Telephony data key"
        fun getTelephonyData(intent: Intent): TelephonyInfo? {
            if (intent.hasExtra(BROADCAST_DATA_KEY)) {
                return intent.getSerializableExtra(BROADCAST_DATA_KEY) as TelephonyInfo
            } else {
                return null
            }
        }
    }

    //inject
    private val logger: Logger = SimpleLogger()

    private lateinit var telephonyManager: TelephonyManager
    private lateinit var stateListener: StateListener

    private val data = MutableLiveData<TelephonyInfo>()
    private val binder: IBinder  by lazy {
        ServiceBinder()
    }

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        super.onCreate()
        telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        stateListener = StateListener()
        onInfoChanged(
            TelephonyInfo(
                telephonyManager.cellLocation, null,
                telephonyManager.allCellInfo,
                if (Build.VERSION.SDK_INT >= 26) telephonyManager.serviceState else null
            )
        )
        startProcess()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY;
    }

    private fun startProcess() {
        telephonyManager.listen(
            stateListener, LISTEN_SIGNAL_STRENGTHS
                    or LISTEN_CELL_INFO or LISTEN_CELL_LOCATION or LISTEN_SERVICE_STATE
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        telephonyManager.listen(stateListener, LISTEN_NONE)
    }

    fun getTelephonyInfo(): LiveData<TelephonyInfo> = data

    override fun onBind(intent: Intent?) = binder

    internal inner class ServiceBinder : Binder() {
        val service: TelephonyService
            get() = this@TelephonyService
    }

    private fun onInfoChanged(newInfo: TelephonyInfo) {
        data.value = newInfo
        //For other listeners how using Broadcasts
        LocalBroadcastManager.getInstance(this)
            .sendBroadcast(Intent(BROADCAST_NAME).apply {
                putExtra(BROADCAST_DATA_KEY, newInfo)
            })
        logger.log(newInfo.toString())
    }

    private inner class StateListener : PhoneStateListener() {
        override fun onCellLocationChanged(info: CellLocation?) {
            super.onCellLocationChanged(info)
            onInfoChanged(data.value!!.copy(location = info))
        }

        override fun onSignalStrengthsChanged(info: SignalStrength?) {
            super.onSignalStrengthsChanged(info)
            onInfoChanged(data.value!!.copy(signalStrength = info))
        }

        override fun onCellInfoChanged(info: MutableList<CellInfo>?) {
            super.onCellInfoChanged(info)
            onInfoChanged(data.value!!.copy(cellInfo = info))
        }

        override fun onServiceStateChanged(info: ServiceState?) {
            super.onServiceStateChanged(info)
            onInfoChanged(data.value!!.copy(serviceState = info))
        }
    }
}
