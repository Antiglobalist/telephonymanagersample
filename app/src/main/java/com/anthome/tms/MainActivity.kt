package com.anthome.tms

import android.app.Service
import android.arch.lifecycle.Observer
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import com.anthome.tms.other.AppSettings
import com.anthome.tms.other.AppSettingsSharedPref
import com.anthome.tms.other.notNull
import com.anthome.tms.services.TelephonyInfo
import com.anthome.tms.services.TelephonyService
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var connection: ServiceConnection
    private lateinit var prefs: AppSettings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prefs = AppSettingsSharedPref(this)
        connection = object : ServiceConnection {
            override fun onServiceDisconnected(p0: ComponentName?) {

            }

            override fun onServiceConnected(p0: ComponentName?, binder: IBinder?) {
                (binder as TelephonyService.ServiceBinder).service
                    .getTelephonyInfo().observe(this@MainActivity,
                        Observer {
                            updateInfo(it)
                        })
            }
        }
        bindService(
            Intent(
                this,
                TelephonyService::class.java
            ), connection, Service.BIND_AUTO_CREATE
        )
        initAutoStartState()
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(connection)
    }

    private fun updateInfo(info: TelephonyInfo?) {
        notNull(info) {
            info_cell.text = it.cellInfo?.toString() ?: ""
            info_location.text = it.location?.toString() ?: ""
            info_service.text = it.serviceState?.toString() ?: ""
            info_signal.text = it.signalStrength?.toString() ?: ""
        }
    }

    private fun initAutoStartState() {
        auto_start.isChecked = prefs.isAutoStartEnabled()
        auto_start.setOnCheckedChangeListener { buttonView,
                                                isChecked -> prefs.setAutoStartEnable(isChecked) }
    }

    //sample of telephony receiver
    private val telephonyReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (TelephonyService.BROADCAST_NAME.contentEquals(intent.action)) {
                val info = TelephonyService.getTelephonyData(intent)
                updateInfo(info)
            }
        }
    }
}
